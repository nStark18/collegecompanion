

DROP TABLE dbo.College_User_Relations;
DROP TABLE dbo.Colleges;
DROP TABLE dbo.CompanionUser;
DROP TABLE dbo.StateList;
DROP TABLE dbo.FinLimitList;
DROP TABLE dbo.PrivacyList;
DROP TABLE dbo.DegreeList;
DROP TABLE dbo.DegreeType;
DROP TABLE dbo.AcceptanceRate;
DROP TABLE dbo.DemoRate;
DROP TABLE dbo.DemoAge;


DROP TABLE [dbo].[AspNetUserClaims];
DROP TABLE [dbo].[AspNetUserLogins];
DROP TABLE [dbo].[AspNetUserRoles];
DROP TABLE [dbo].[AspNetUsers];
DROP TABLE [dbo].[AspNetRoles];

GO
